# Alupark.fi

## Prerequisites:

- [Hugo](http://gohugo.io/): `brew install hugo`
- `npm install && gulp`

## Development:

- `gulp serve`

## Run:

- `gulp docker-run`
