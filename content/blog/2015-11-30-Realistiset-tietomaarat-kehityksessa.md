+++
date = "2015-11-30T21:13:05+03:00"
draft = false
title = "Realistiset tietomäärät kehityksessä"
slug = "Realistiset tietomaarat kehityksessa"
author = "anttij"
thumbnail = "/images/blog/categories/development.jpg"
+++

Minkälaiselle tietomäärälle tietojärjestelmäsi on suunniteltu? Valitettavan usein asiaa ei ole huomioitu lainkaan tai analyysin tuloksia ei ole otettu huomioon muussa määrittelyssä, kehityksessä ja testauksessa. Usein tietomäärien vaikutukset nousevat esille vasta projektin loppuvaiheessa tehtävässä suorituskykytestauksessa. Vasta tässä vaiheessa löytyvät ongelmat ovat kuitenkin usein hankalia ja kalliita korjata.

Tyypillisesti tietomäärien aiheuttavat ongelmat ovat suorituskykyongelmia. Esimerkiksi tietokantakyselyiden hidastumista ja näkymien hidasta aukeamista. Pahimmillaan suuret tietomäärät voivat kaataa järjestelmän kokonaan. Toinen, usein ylenkatsottu ongelma, ovat käyttöliittymäsuunnitteluun liittyvät kysymykset. Joidenkin käsitteiden oletettua suurempi määrä voi johtaa hankaliin käytettävyysongelmiin. On esimerkiksi hyvin eri asia valita listalta kymmenen rivin joukosta kuin sadan rivin joukosta. Lisäksi osa ongelmista on näiden kahden tyypin yhdistelmiä kuten esimerkiksi sivuutukseen liittyvät ongelmat.

## Ratkaisuehdotus ##
Aikainen keskustelu tietomääristä nostaa esiin tärkeitä kysymyksiä siitä, miten järjestelmää tullaan käyttämään. Joidenkin käsitteiden osalta arviota yleensä tehdään projektin alkuvaiheessa liiketoiminnallisista näkökulmista. Nämä tarkastelut rajautuvat kuitenkin yleensä vain liikevaihtoon ja kuluihin vaikuttaviin käsitteisiin. Tietomääräanalyysi on tärkeää tehdä jo projektin alkuvaiheessa yhdessä asiakkaan tai muun lopputuotteen edustajan kanssa, jotta sen tulokset ovat kaikkien käytössä alusta asti. Yhdessä tehty tietomääräanalyysi ohjaa myös kaikkien osapuolien odotuksia järjestelmän suorituskyvystä oikeaan suuntaan.

Yksinkertainen tapa huomioida tietomäärät kehityksessä on generoida jo kehityksen alkuvaiheesta alkaen realistinen määrä testidataa, jota kaikki kehittäjät velvoitetaan käyttämään. Näin tietomääriin liittyvät ongelmat nousevat esiin jo alkuvaiheessa ja kaikki ratkaisut tehdään alusta asti siten, että ne toimivat oikeilla tietomäärillä.

Tietomäärien määrittely jakautuu käsitteiden lukumäärien määrittelyn ja transaktioiden lukumääriin määrittelyyn. Tarkastelun aluksi tulee kiinnittää tarkastelun aikajänne. Aikajänteenä voi toimia esimerkiksi järjestelmän elinkaari. Tietomäärien osalta määritellään alkutilanne ja muutosnopeus. Alkutilanne on relevantti, jos tietoa siirretään olemassa olevasta järjestelmästä. Muutosnopeuden aikayksikkö tulee määritellä tilanteen mukaan. Muutosnopeus voidaan määritellä esimerkiksi vuosittain syntyvien ja poistuvien tietojen kautta.

Tietomääräanalyysista saadut luvut on usein mielekästä kertoa jollakin varmuuskertoimella, kun niiden avulla generoidaan testidataa tai suunnitellaan suorituskykytestejä. Varmuuskerrointa voidaan muuttaa kehityksen kuluessa esimerkiksi siten, että ensimmäisissä sprinteissä pyritään 50% datamäärään, jonka jälkeen siirrytään 200% datamäärään. Varmuuskerroin korjaa sekä tietomääräanalyysissa tapahtuneita virheitä sekä testiaineiston ja suorituskykytestien mahdollisia epäedustavuuksia tiedon muodossa ja käyttötavoissa.

Huomioimalla tietomäärät projektin alusta alkaen vältetään ikävät yllätykset projektin loppuvaiheessa.

1. Analysoi järjestelmän tietomäärät
1. Määritä varmuuskerroin
1. Ota realistinen tietomäärä käyttöön kaikessa kehityksessä ja testauksessa.

## Esimerkki ##

Käytetään tarkasteluesimerkkinä yksinkertaista järjestelmää, jossa opettajat voivat viestiä oppilaiden kanssa. Vuonna 2013 Suomessa oli OAJ:n mukaan 39 000 perusopetuksen opettaja. Samaan aikaan perusopetuksen oppilaita oli noin 550 000. Esimerkkijärjestelmässä on vain yksi transaktio: viestin lähettäminen. Oletetaan, että jokaista oppilasta kohden lähetetään yksi viesti viikossa. Oletetaan lisäksi, että jokaiseen lähetettyyn viestiin tulee yksi vastaus. Oletetaan yksinkertaisuuden vuoksi, että viestejä ei poista. Otetaan tarkastelun aikajänteeksi 5 vuotta ja määritetään järjestelmän kasvuvauhti siten, että ensimmäisenä vuonna markkinaosuus on 1% ja se kasvaa vuosittain 5%-yksikköä.

Tässä kohtaa on hyvä testata intuitiota ja miettiä, kuinka monta viestiä järjestelmässä on viiden vuoden kuluttua? Tuhansia, satoja tuhansia, miljoonia? Tekemällä analyysi huomataan, että viestejä kertyy viidessä vuodessa niin paljon, että sillä todennäköisesti alkaa olemaan jo jonkinasteinen suorituskykyvaikutus, jos asiaa ei huomioida.

![Taulukko datamääristä](/images/blog/Realistiset-tietomaarat-kehityksessa/taulukko.png)

[Taulukko Google Spreadsheettinä](https://docs.google.com/spreadsheets/d/1F1-lA-XUviNq-manPSgnTT39Mg5wBwrksu2K74whiPE/edit?usp=sharing).
