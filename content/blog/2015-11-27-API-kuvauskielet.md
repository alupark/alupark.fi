+++
author = "anttij"
date = "2015-11-24T06:56:22+02:00"
draft = true
slug = "API-kuvauskielet"
title = "TYÖNIMI API-kuvauskielet"
+++

# VAIHDA PÄIVÄMÄÄRÄ #

API (Application Programming Interface) kääntyy sujuvasti ohjelmistorajapinnaksi. Karkeasti kuvattuna API kuvaa, miten ohjelmistokomponentit voivat viestiä keskenään. API on ohjelmistokomponentin julkinen rajapinta.

**Tämä kappale vaatii selkeyttämistä**
API:t voidaan jakaa kahteen kategoriaan: ohjelmistokirjastoihin ja etäkutsurajapintoihin. Ohjelmistokirjasto on lähdekoodista tietylle ohjelmointikielelle ja alustalle käännetty komponentti, joka tarjoaa ohjelmoijalle mahdollisuuden käyttää esimerkiksi jotakin laitetta ohjelmointialustan keinoin. Etäkutsurajapinnat taas tarjoavat joko standardiin tai itse kehitettyyn etäkutsuprotokollaan pohjautuvan tavan viestiä ohjelmistokomponentin kanssa. Usein ohjelmoijan tehtäväksi jää integroida etäkutsurajanpinta osaksi muuta järjestelmää.

API-dokumentaatio tehtävä on kuvata tietyn ohjelmistokomponentin tarjoamat operaatiot ja niiden syötteet ja paluuarvot. API-dokumentatiossa kuvataan lisäksi syötteisiin ja paluuarvoihin liittyvät tietotyypit ja rajoitteet. Lisäksi hyvä API-dokumentaatio kertoo myös mihin operaatioita käytetään.

## Swagger ##

Lisenssi Apache License 2.0.

## API Blueprint ##

## RAML ##
