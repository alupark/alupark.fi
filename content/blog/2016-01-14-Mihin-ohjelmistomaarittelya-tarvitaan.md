+++
author = "anttij"
date = "2016-01-25T20:00:22+02:00"
draft = false
slug = "Mihin ohjelmistomaarittelya tarvitaan"
title = "Mihin ohjelmistomäärittelyjä tarvitaan?"
thumbnail = "/images/blog/categories/business.jpg"
+++

Ohjelmistomäärittelyitä ovat vaatimusmäärittelyt, toiminnalliset kuvaukset, suunnitteludokumentit, tekniset kuvaukset sekä käyttö- ja ylläpito-ohjeet. Toisaalta määrittelyitä ovat usein myös palaverimuistiinpanot, sähköpostiketjut ja käytäväkeskustelut. Määrittelyt ovat myös muodoltaan moninaisia. Ne voivat olla listoja, taulukoita, muodollisia dokumentteja, kaaviota, piirroksia tai valokuvia fläppitauluista. Yhteistä niille on kuitenkin se, että ne pyrkivät omalla tavallaan kuvaamaan joko sitä millainen ohjelmiston tulisi olla tai sitä millainen se on. On selvää että edeltävät esimerkit eivät kata kaikkia ohjelmistomäärittelyn muotoja sillä tässäkin voidaan siteerata Charles Darwinin Lajien synnyn kuuluisaa lausetta: *"endless forms most beautiful and most wonderful"*. Käytän termiä *ohjelmistomäärittely*, mutta sen voi halutessaan korvata sanalla *järjestelmämäärittely*. Lainalaisuudet pysyvät samoina.

Määrittelyillä on kolme pääasiallista tehtävää, kommunikointi, todentaminen ja dokumentointi. Erilaisten määrittelyiden tarve on aina syytä miettiä projektikohtaisesti ja se voi myös muuttua projektin kestäessä. Tärkeimpänä asiana määrittelyitä tehtäessä on miettiä kuka on määrittelyn lukija eli kenelle se kirjoitetaan. Tämä on yleinen periaate, joka koskee tietysti kaikkea viestintää. Jos määrittelylle ei löydy potentiaalista lukijaa, niin sitä ei todennäköisesti pidä kirjoittaa.

>Jos määrittelylle ei löydy potentiaalista lukijaa, niin sitä ei todennäköisesti pidä kirjoittaa.

Tarve määrittelyiden käyttöön kommunikaatiotyökaluna korostuu, kun projektissa on mukana useita tiimejä, useita toimittajia, ei-näkyviä toiminnallisuuksia tai asiakasyhteistyön malli, jossa vaaditaan määrittelyyn sitoutumista ennen toteuttamista. Kommunikaation helpottamiseksi laaditulla määrittelyllä on olemassa hinta, joka syntyy sen kirjoittamisesta, lukemisesta, kommentoinnista ja korjaamisesta. Väitän kuitenkin, että määrittelyn tekemiseen käytetty työ maksaa itsensä takaisin, jos sillä on yksikin lukija.

> Määrittelyn tekemiseen käytetty työ maksaa itsensä takaisin, jos sillä on yksikin lukija.

Määrittelyiden käyttö ohjelmiston vaatimustenmukaisuuden todentamiseen kuuluu oleellisena osana joihin asiakasyhteistyömalleihin. Määrittelyketju "vaatimusmäärittely → toiminnallinen kuvaus → testitapaukset → testitulokset", mahdollistaa ainakin teoriassa ohjelmiston vaatimustenmukaisuuden toteamisen, kun jokaisen ketjun osan on todettu vastaavan edeltävää osaa. Prosessi on raskas ja vaatii tarkkuutta sekä määrittelyiden laadinnassa että niiden hallinnoinnissa. Useimmissa projekteissa on kuitenkin tilanteita, joissa määrittelyitä käytetään jossakin vaiheessa projektia vastaamaan kysymykseen siitä, onko tehty sitä mitä on sovittu. Yleensä nämä keskustelut nojaavat erilaisiin epämuodollisiin muistioihin tai sähköposteihin ja siten niihin tulisi kaikkien osapuolienkin suhtautua. Se, millä tavalla määrittelyt ovat eri osapuolia velvoittavia, on syytä kirkastaa kaikille osapuolille, jotta vältytään myöhemmin hankalilta kiistoilta asiaan liittyen.

Määrittelyiden tehtävä dokumentoinnin työkaluna on huomattavasti hankalampi. Jos määrittelyä käytetään dokumentointiin, niin määrittely tulee aina pitää ajan tasalla ohjelmiston kulloisenkin tilan kanssa. Määrittelyt vastaavat usein kysymyksiin: "Miksi?", "Mitä?" ja "Miten?". Määrittelyt kehittyvät usein tässä samassa järjestyksessä ja projektin määrittelyn taso vaikuttaakin huomattavasti siihen, kuinka moneen määrittelyyn muutokset vaikuttavat. Mitä enemmän määrittelyissä on vastattu myös kysymykseen "Miten?", niin sitä todennäköisempää on, että mikä tahansa muutos aiheuttaa muutoksen myös määrittelyihin.

Määrittelyitä kuitenkin tarvitaan dokumentointitarpeisiin, jos ohjelmisto on laaja, sen elinkaari pitkä tai sen ylläpitoon osallistuu eri ihmisiä kuin sen toteuttamiseen.  Määrittelyiden käytöllä dokumentoinnin välineenä on hinta, joka on luonteeltaan erilainen kuin niiden alkuperäisen laatimisen hinta. Itse määrittelyiden säilyttäminen ja versiointi on hallinnollista työtä, joka täytyy tehdä, jotta määrittelyt voivat toimia dokumentaationa. Lisäksi jokaisen tuotteen muutoksen yhteydessä tulee miettiä aiheuttaako se muutoksia olemassa oleviin määrittelyihin. ja tehdä tarvittavat muutokset. Dokumentaatioiksi luotujen määrittelyiden kohtalona on valitettavan usein se, että ne eivät koskaan enää tule luetuiksi.

Tässä kirjoituksessa on tehty pintaraapaisu siihen, mihin ohjelmistomäärittelyitä tarvitaan. Tällä hetkellä näyttää siltä, että tämä kirjoitus aloittaa omalta osaltani sarjan erilaisiin määrittelyasioihin keskittyviä kirjoituksia eli ei muuta kuin speksaushousut jalkaan ja silmälaseja ylemmäksi nenänvarrella.
