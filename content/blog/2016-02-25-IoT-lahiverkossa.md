+++
author = "anttij"
date = "2016-03-01T12:15:22+02:00"
draft = false
slug = "IoT-laitteet turvallisesti lahiverkossa"
title = "IoT-laitteet turvallisesti lähiverkossa"
thumbnail = "/images/blog/categories/development.jpg"
+++

Kukaan ei ole voinut kokonaan välttyä Asioiden Internetiltä, Internet of Thingisiltä tai tuttavallisemmin IoT:ltä. IoT elää tällä hetkellä nousukautta ja markkinoilla on paljon eri periaatteilla toimivia laitteita ja toisaalta vähän standardisaatiota. IoT on laaja kenttä ja keskityn tässä vain lähiverkkoteknologioita (Ethernet, WiFi) kommunikaatiossa käyttäviin kuluttajatuotteisiin.

IoT-laitteista on löydetty haavoittuvuuksia mm. Suomessa myytävistä [etäohjattavista pistorasioista](http://www.digitoday.fi/tietoturva/2016/01/19/suomessa-myytava-pistorasia-kaapattavissa-netista-kasin/2016648/66) ja markkinatilanne indikoi, että laitteiden laatu ei aivan hetkeen ole myöskään parantumassa. IoT-laitteet ovat sikäli loistava hyökkäysvektori potentiaaliselle pahantekijälle, että ihmiset hyväksyvät helposti niiden olevan "mustia laatikoita" ja niiden hyviin ominaisuuksiin kuuluu usein myös helppo asennettavuus, joka saa ihmiset unohtamaan niiden uhat.

Tässä kirjoituksessa on jonkin verran perustason verkkotekniikkaa. Blogikirjoituksen yhteydessä ei pysty valitettavasti avaamaan kaikkia protokollia ja termejä täydellisesti, mutta olen pyrkinyt linkittämään ne asianmukaisiin lähteisiin. Olen vapaasti käyttänyt mm. termiä IP-protokolla vaikka se kääntyykin suoraan internetprotokolla-protokollaksi.

**TL;DR**: Jos haluat vain lukea, miten kytkeä laitteet turvallisesti, niin hyppää kohtaan *[Toimiva ratkaisu](#ratkaisu)*.

## IoT-laitteista

IoT-laitteet voidaan jakaa toimintaperiaatteen mukaan karkeasti kolmeen kategoriaan:

- IoT-laite toimii sensorina ja lähettää mittausdataa internetissä olevalle palvelimelle (client push). Esimerkiksi lämpömittarit ja yksinkertaiset valvontakamerat.
- IoT-laite hakee ohjauskomentoja internetissä olevalta palvelimelta ja toimii niiden mukaisesti (client pull). Esimerkiksi auton moottorilämmitys tai etäohjattava ovi.
- IoT-laite toimii palvelimena muille verkossa oleville laitteille, jotka voivat joko pyytää siltä tietoja tai antaa sille ohjauskomentoja.

Viimeisen kategorian laitteiden ongelma on se, että ohjaavan laitteet eli tietokoneen tai puhelimen tulee joko olla samassa lähiverkossa IoT-laitteen kanssa tai reitittimelle tulee erikseen tehdä yhteysavaus julkiverkosta laitteelle, jotta niitä voidaan käyttää internetin yli. Tästä syystä oletan, että valtaosa laitteista kuuluu kahteen ensimmäiseen kategoriaan. Useimmat molempiin.

Yritän esittää ratkaisun, joka mahdollistaa näiden laitteiden kytkemisen turvallisesti lähiverkkoon ja joka kuitenkin vaatii mahdollisimman vähän konfiguraatiota ja on myös halpa toteuttaa. Artikkelin lopussa esittelen vielä lyhyesti muita tapoja ratkaista ongelma.

## Alkutilanne

Tyypillisessä kotiverkossa on verkko-operaattorilta saatu reititin, jossa on [NAT-toiminto](https://en.wikipedia.org/wiki/Network_address_translation). Yksinkertaistettuna reitittimen toisella puolella on julkinen internet, jossa reitittimellä on yksi IP. Toisella puolella on lähiverkko, jossa reitittimellä on toinen IP eli niin sanottu [yhdyskäytävä-IP](https://en.wikipedia.org/wiki/Default_gateway). Kun reitittimeen kytketyt laitteet haluavat kommunikoida oman lähiverkkonsa ulkopuolelle ne tekevät sen reitittimen kautta. Reitittimen NAT-toiminto välittää liikenteen siten, että alkuperäinen lähettäjä peittyy ja ulospäin näyttää siltä kuin reititin kommunikoisi julkiverkon palvelimen kanssa. Sama NAT-toiminto estää myös sen, että julkiverkosta ei voida ottaa yhtettä sisäverkon laitteisiin. NAT on kotiverkon tärkein turvamekanismi.

![NAT](/images/blog/IoT/NAT.png)
*Kuvassa viivan päässä oleva pallo edustaa reitittimen WAN-porttia eli verkkoa, johon se reitittää liikennettä.*

## Oletukset

- Reitittimet ja niiden NAT-toiminnallisuudet ovat virheettömiä
- IoT-laitteet ovat ilkeämielisiä ja hyökkääjä pystyy kontrolloimaan niitä mielivaltaisesti
- Muut verkon tietokoneet ovat luotettavia. Ne ja niiden käsittelemä tieto yritetään suojata ulkopuolilta.

## Ensimmäinen epäonnistuminen

Yksinkertaisin tapa liittää IoT-laitteet lähiverkkoon on liittää ne samaan verkkoon muiden laitteiden kanssa eli käyttää vain yhtä reititintä.

![Ensimmäinen epäonnistuminen](/images/blog/IoT/Error1.png)
*Kuvassa hehkulamppu edustaa IoT-laitetta.*

Tämä sijoittelu on turvaton ilmiselvistä syistä. Ollessaan samassa verkossa suojattavien laitteiden kanssa ne voivat suorittaa erilaisia porttiskannauksia ja muita hyökkäysyrityksiä verkon laitteita vastaan.

Esimerkiksi valtiovarainministeriön [VAHTI 3/2010 Sisäverkko-ohje](https://www.vahtiohje.fi/web/guest/3/2010-sisaverkko-ohje), joka ohjeistaa valtiollisia toimijoita, toteaa yksikäsitteisesti kaikille toimintaympäristöille pakollisessa vaatimuksessa 5.5 seuraavasti:

>Eri verkot tai niiden osat on eristetty toisistaan loogisesti tai fyysisesti.

## Toinen epäonnistuminen

Korjausyrityksenä ensimmäiseen epäonnistumiseen lisätään verkkoon toinen reititin, joka muodostaa erillisen verkon. Kytketään tämä tämän reitittimen WAN-portti ensimmäisen reitittimen LAN-porttiin. Tällä tavalla kytkettynä IoT-laitteet ovat eristettynä omaan lähiverkkoonsa, mutta ne voivat edelleen kommunikoida julkiverkkoon kahden reitittimen läpi.

![Toinen epäonnistuminen](/images/blog/IoT/Error2.png)

Tässä kytkentätavassa on kuitenkin ongelma. Ilkeämielinen IoT-laite voi esimerkiksi käyttää `traceroute` -komentoa johonkin julkiverkon osoitteeseen ja saada seuraavanlaisen vastauksen:

```
> traceroute 8.8.8.8
traceroute to 8.8.8.8 (8.8.8.8), 64 hops max, 52 byte packets
 1  10.0.0.1 (10.0.0.1)  1.006 ms  0.415 ms  0.312 ms
 2  192.168.0.1 (192.168.0.1)  5.181 ms  6.559 ms  1.199 ms
 3  216.239.54.181 (216.239.54.181)  24.038 ms  23.481 ms  24.968 ms
```

Ensimmäinen hyppy on IoT-laitteen oman verkon reitittimen yhdyskäytäväosoite ja toinen hyppy on suojattavan verkon reitittimen yhdyskäytäväosoite. Hyökkääjä saa tätä kautta tietoonsa suojattavan verkon IP-numeroinnin ja pystyy lähettämään IP-paketteja tuolle osoitealueelle. IoT-verkon reititin reitittää ne paketit ulospäin, koska ne eivät osu sen osoitealueelle, mutta suojattavan verkon reititin reitittää paketit omaan verkkoonsa, koska ne ovat sen osoitealueella. Tulee huomata, että konfiguraatio itsessään toimii oikein eli laitteissa ei sinällään ole mitään vikaa, mutta se ei ole turvallinen tapa sijoittaa IoT-laitteita lähiverkkoon.

## Kolmas epäonnistuminen

Yritetään korjata toinen epäonnistuminen siirtämällä internetyhteys kulkemaan ns. IoT-reitittimen kautta ja vaihtamalla reititinten välinen WAN/LAN -yhteys toisinpäin. Tässä mallissa verkkojen luotettavuus siis laskee suojatusta verkosta ulospäin mentäessä. Ajatuksena on, että suojattavan verkon reitin suojelee verkossa olevia laitteita ulkopuolisilta IoT-laitteilta samalla tavalla kuin se peruskokoonpanossa suojelee laitteita internetissä olevilta uhilta.

![Kolmas epäonnistuminen](/images/blog/IoT/Error3.png)

Tämä kytkentätapa poistaa edellisen kytkennän ongelmat, mutta luo uudenlaisen ongelman. Se, että suojattavan verkon liikenne kulkee julkiverkkoon IoT-verkon kautta pitäisi jo itsessään aiheuttaa kulmakarvojen nousemista, mutta pystyykö ilkeämielinen IoT-laite hyödyntämään sitä?

Edellinen hyökkäys suoritettiin käyttämällä IP-tason toiminnallisuuksia, mutta nyt meidän täytyy laskeutua alaspäin [OSI-mallissa](https://en.wikipedia.org/wiki/OSI_model) [Ethernet-tasolle](https://en.wikipedia.org/wiki/Ethernet).

Ethernet-protokolla vastaa matalalla tasolla pakettien välittämisestä laitteiden välillä. Ethernet-protokollassa jokaisella laitteella on globaalisti uniikki [MAC-osoite](https://en.wikipedia.org/wiki/Media_access_control) ja liikenne on lähtökohtaisesti point-to-point -tyyppistä eli suoraan kahden laitteen välistä. Tehdäkseen hypyn IP-tasolta Ethernet-tasolle laitteiden täytyy saada selville mikä IP-osoite vastaa mitäkin MAC-osoitetta. Käytännössä tämä tehdään käyttäen [ARP-protokollaa](https://en.wikipedia.org/wiki/Address_Resolution_Protocol), joka on osa Ethernet-protokollaa. Tarkastellaan protokollan toimintaa suojattavan verkon ja IoT-verkon välissä olevan reitittimen käynnistyessä:

1. Reititin A käynnistyy ja sille on konfiguroitu IP 10.0.0.250 ja yhdyskäytävän IP:ksi 10.0.0.1.
2. Reititin A lähettää "Kenellä on IP 10.0.0.1?" ARP-paketin erityiseen [broadcast-MAC -osoitteeseen](https://en.wikipedia.org/wiki/Broadcast_address), joka välittyy kaikille verkossa oleville laitteille.
3. Reititin B julkiverkon ja IoT-verkon välissä vastaanottaa paketin ja vastaan ARP-paketilla, jossa se ilmoittaa oman MAC-osoitteensa.
4. Reititin A voi välittää Ethernet-paketteja reititin B:lle.

Valitettavasti ilkeämielinen IoT-laite voi kuitenkin häiritä tätä prosessia vastaamalla kohdassa 3. omistavansa kysytyn IP-osoitteen ja tekeytyvänsä näin yhdyskäytäväksi. Tämän jälkeen IoT-laite vastaanottaa kaiken verkosta ulospäin suuntautuvan liikenteen ja voi analysoida sitä mielivaltaisesti. IoT-laite luonnollisesti välittää liikenteen eteenpäin oikealle yhdyskäytävälle, jolloin sen toimintaa on hankalampi havainnoida. Hyökkäyksestä käytetään termiä [ARP spoofing](https://en.wikipedia.org/wiki/ARP_spoofing).

<a name="ratkaisu"></a>
## Toimiva ratkaisu

Kaikki edelliset ongelmat voidaan välttää kytkennällä, joka on yksinkertaisimmassa muodossaan Y:n muotoinen. Juurireititin on yhteydessä julkiverkkoon ja siihen liittyvät reitittimet erottavat eri verkot toisistaan. Juurireitittimen alla on kuvassa vain kaksi reititintä, mutta niitä voidaan järjestelmään lisätä enemmänkin. Voidaan esim. muodostaa erillinen vieralijaverkko tai oma verkkonsa fyysisen turvallisuuden kannalta kriittisille laitteille kuten lukoille ja ilmastoinnin ohjaukselle. On olennaista, että juurireitittimeen itseensä ei kytketä muita laitteita kuin toisia reitittimiä.

![Onnistuminen](/images/blog/IoT/Correct.png)

Tämä ratkaisu on lähes konfiguraatiovapaa. Reitittimet voidaan useimmissa tapauksissa kytkeä näin suoraan hyllystä. Kunhan kunkin reitittimen WAN-portista kulkee liikenne juurireitittimen LAN-porttiin ja reitittimissä on NAT-toiminto päällä eivätkä ne siis ole siltaavassa moodissa.

Haittapuolena on toki se, että suojattavassa verkossa olevilla laitteilla ei voida suoraan kommunikoida IoT-verkossa olevien laitteiden kanssa. Jos tällaista toiminnallisuutta tarvitaan, voidaan suojattavan verkon tietokone tai älypuhelin liittää väliaikaisesti IoT-verkkoon tai vielä mieluummin liittää verkkoon kokonaan erillinen konfiguraatioterminaali.

## Muut vaihtoehdot

Vastaavanlainen suojaus voidaan saada aikaan mm. käyttämällä kalliimpien yrityskäyttöön suunniteltujen kytkinten [VLAN-ominaisuuksia](https://en.wikipedia.org/wiki/Virtual_LAN), joilla yksi fyysinen verkko voidaan jakaa erillisiin virtuaaliverkkoihin tai konfiguroimalla kytkimen IP-palomuuri siten, että se sallii vain tietyt yhteydet. Tämä vaatii kuitenkin huomattavasti enemmän ylläpitoa, kalliimpaa laitteistoa ja vaatii mahdollisesti myös uudelleenkonfigurointia, kun verkkoon liitetään uusia laitteita.

*Tämä blogikirjoitus on vahvasti [Security Now! -podcastin](https://www.grc.com/securitynow.htm) jakson [#545 Three Dumb Routers](https://www.grc.com/sn/sn-545.htm) inspiroima.*

*Hehkulamppuikoni on [LGPL-lisensoitu](http://www.gnu.org/licenses/lgpl.html) ja sen on tehnyt [everaldo.com](http://www.everaldo.com/).*
