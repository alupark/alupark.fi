+++
author = "anttij"
date = "2016-03-09T10:00:00+02:00"
draft = false
slug = "Apple iOS tietoturvasta osa 1"
title = "Apple iOS:n tietoturvasta (osa 1)"
thumbnail = "/images/blog/categories/security.jpg"
+++

Tämä kirjoitus perustuu Applen syyskuussa 2015 julkaisemaan 60-sivuiseen white
paperiin [iOS Security - iOS 9.0 or later](https://www.apple.com/business/docs/iOS_Security_Guide.pdf). Kirjoituksen tarkoituksena ei ole käsitellä koko dokumenttia ja iOS-laitteiden tietoturvaa vaan nostaa sieltä esille mielenkiintoisia ratkaisuja ja myös mahdollisia ongelmia.

Apple on julkaissut myös aiempia versiota kyseistä dokumentista, mutta keskityn tässä kirjoituksessa ainoastaan uusimpaan versioon. Pidättäydyn myös käsittelemään Applen laitteita, jotka käyttävät vähintään käyttöjärjestelmäversiota iOS 9.0 ja vähintään A7-sukupolven järjestelmäpiiriä. Käytännössä tämä tarkoittaa puhelinversiota iPhone 5s tai uudempaa eli puhelimia, joissa on sormenjälkiskanneri (Touch ID). Vaikka kirjoitan vain puhelimista, niin kaikki kirjoitettu pätee rajoituksin myös muihin iOS-laitteisiin eli iPadeihin ja iPod Touchiin.

iPhonejen tietoturva on viime aikoina ollut paljon otsikoissa, kun FBI haki [oikeudenmääräyksen](https://assets.documentcloud.org/documents/2714005/SB-Shooter-Order-Compelling-Apple-Asst-iPhone.pdf), jonka mukaan Applen tulee avustaa sitä San Bernardino terroristi-iskun puhelimen avaamisessa. Hyökkääjän puhelin oli iPhone 5c eli siinä ei ole kaikkia tässä kirjoituksessa tarkasteltuja tietoturvamekanismeja kuten Secure Enclavea. Oikeuden määräys on sen laatuinen, että Apple pystyisi halutessaan teknisesti toteuttamaan FBI:n toiveen. Tapaus on aiheuttanut paljon julkista keskustelua ja Applen toimitusjohtaja Tim Cook kirjoitti pyyntöön myös [julkisen vastauksen](http://www.apple.com/customer-letter/). Tapauksen käsittely on vielä kesken, koska Apple on [valittanut tuomiosta](https://www.justice.gov/usao-cdca/file/826836/download).

## Rautatason ratkaisut ##

Monet tietoturvamekanismit on rakennettu jo rautatasolle. iPhone 5s:stä eteenpäin iOS-laitteissa on ollut erillinen järjestelmäpiiri Secure Enclave, joka on erotettu käynnistymisprosessia myöten varsinaisesta järjestelmäpiiristä, jolla käyttöjärjestelmä ja sovelluskoodi ajetaan. Secure Enclave vastaa useimpien tietoturvamekanismien matalan tason toteutuksesta. Kommunikaatio Secure Enclaven kanssa tapahtuu erillisen jaetun muistialueen (ns. Mailbox) kautta, jonne molemmat järjestelmäpiirit voivat kirjoittaa ja josta ne voivat lukea. Lisäksi piireillä on jaettu keskeytys, jolla voidaan kertoa, että uusi viesti on saatavilla. Tällä tavalla piirit on vahvasti erotettu toisistaan. Vaikka hyökkääjä pystyisikin ajamaan mielivaltaista sovelluskoodia varsinaisella piirillä hän ei pysty vaikuttamaan Secure Enclaven toimintaan ja siten kiertämään turvamekanismeja. Secure Enclaven ohjelmisto voidaan päivittää erikseen, mutta julkisissa dokumenteissa ei ole tietoa siitä, millä tavalla ja missä tilanteissa se voidaan tehdä. Jos päivitys esimerkiksi voidaan tehdä laitteen ollessa lukittuna, avaa se potentiaalisia hyökkäysreittejä, joita ainakin Apple voi käyttää.

![Mailbox](/images/blog/iOS_Security/mailboxing.png)

Laitteeseen on valmistuksen yhteydessä tallennettu luettavissa olevat, mutta muuttumattomat, Applen juurisertifikaatti ja ECID-avain. ECID on jokaiselle laitteelle uniikki, mutta se ei sinällään ole salainen vaan voidaan lukea ohjelmallisesti. Lisäksi laitteella on valmistuksen yhteydessä luotu sekä varsinaiselle järjestelmäpiirille sekä Secure Enclavelle UID (Devide Unique ID) ja GID (Device Group ID). Molemmat ovat 256-bittisiä [AES-avaimia](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard). UID on molemmille piireille ja jokaiselle laitteelle uniikki ja se ei ole Applen tiedossa. GID on jokaiselle prosessoriperheelle yhteinen ja on Applen tiedossa. UID:tä tai GID:tä ei voida suoraan lukea vaan niitä käytetään ainoastaan osana kryptografisia funktioita ja avaintenmuodostusta.

iOS-laitteissa on rautatasolla toteutettu satunnaislukugeneraattori, joka saa entropiansa alkujaan käynnistymisvaiheen mikrotason viiveistä ja myöhemmin mm. prosessorin keskeytyksista ja eri sensorien keräämästä datasta (kiihtyvyysanturit, kompassi, radion kantoaalto).

## Levynsalaus ##

Levyosio, jossa käyttäjän tiedot ovat, on aina kryptattu (*full disk encryption*) ja laitteessa on erillinen AES-256 kryptopiiri massamuistin ja keskusmuistin välisessä DMA-väylässä, jolla kryptaukseen liittyvät toimenpiteet voidaan toteuttaa tehokkaasti. Levynsalaus on monitasoinen ja sen toiminta riippuu osin siitä, onko laitteeseen asetettu pääsykoodi vai ei. Perustasolla laitteen asennuksen yhteydessä luodaan UID:hen perustuva salausavain, joka tallennetaan laitteen pysyväismuistiin erityiselle tyhjennettävän muistin alueelle (*effaceable storage*). Tätä aluetta voidaan käsitellä matalan tason levyohjaimen avulla siten, että se voidaan tyhjentää niin, että sen aiempi sisältö ei enää ole luettavissa. Perustason levynsalaus ei vielä tarjoa yksityisyydensuojaa, koska avaimet on tallennettu suoraan levylle, mutta se mahdollistaa tehokkaan laitteen etätyhjennyksen. Levyn käyttäjäosio saadaan tehtyä lukukelvottomaksi poistamalla avaimet tyhjennettävän muistin alueelta, jolloin levyn sisältöä ei pystytä enää dekryptaamaan.

Koska salausavaimen luonnissa on käytetty laitteen yksilöllistä UID:ta, jota ei voida lukea ulkopuolelta, kaikki salausoperaatiot tulee tehdä kyseissä laitteessa. Tämä estää hyökkäykset, jossa laitteen muistipiirit siirrettäisiin toiseen laitteeseen hyökkäyksen toteuttamista varten.

## Käyttöjärjestelmän lataaminen ja päivitykset ##

Järjestelmän käynnistymisessä on neljä vaihetta. Ensimmäisenä ladataan järjestelmäpiiriin valmistuksen yhteydessä ohjelmoitu Boot ROM, jota nimensä (Read-Only Memory) mukaisesti ei voida päivittää. Boot ROM sisältää Applen juurisertifikaatin julkisen avaimen, jonka avulla se varmistaa matalan tason bootloaderin (*Low-Level Bootloader, LLB*) aitouden ja käynnistää sen. LLB tekee matalan tason alustustoimia ja näyttää mm. Applen logon puhelimen käynnistyessä. Viimeinen latausohjelma on iBoot, jonka aitouden LLB varmistaa. iBoot varmistaa iOS-kernelin aitouden ja lataa sen. Jos jokin aitoustarkastus epäonnistuu, niin puhelin menee epäonnistumisen vaiheesta riippuen joko palautustilaan (*recovery mode*) tai DFU-tilaan (*Device Firmware Upgrade*). Molemmissa tapauksissa puhelin tulee kytkeä iTunesiin ja palauttaa tehdasasetuksille.

Käyttöjärjestelmän päivitykset validoidaan samalla tavalla kuin järjestelmän käynnistyessäkin. Lisäksi järjestelmässä on erillinen mekanismi, jolla pyritään estämään nk. *downgrade*-hyökkäykset, joissa hyökkääjä pyrki yrittää päivittää laitteen vanhempaan ohjelmistoversioon, jossa olevia tunnettuja heikkouksia hän sitten pystyisi hyödyntämään. Päivitysyrityksen yhteydessä laite lähettää Applelle viestin, jossa on mukana tiedot niistä ohjelmiston osista jotka aiotaan päivittää, pyynnölle uniikki satunnaisluku (nonce) ja laitteelle uniikki ECID. Applen palvelin varmistaa, että päivitys on sallittu ja palauttaa vastauksen, joka on kryptografissti liitetty sekä pyytävään laitteeseen (ECID) että kyseiseen pyyntöön (nonce). Näin voidaan välttää ohjelmiston kopiointi laitteesta toiseen tai vanhojen päivityslupien uudelleenkäyttö.

### Pääsykoodit ja TouchID ###

Parantaakseen laitteen tietoturvaa käyttäjiä kannustetaan käyttämään laitteessaan pääsykoodia. Pääsykoodista muodostetaan yhdessä UID:n kanssa avain, jolla voidaan edelleen salata tiedostoja (tästä enemmän blogikirjoituksen 2-osassa). Avaimenmuodostus käyttää iteroivaa [avaintenmuodostusfunktiota](https://en.wikipedia.org/wiki/Key_derivation_function) siten, että avaimenmuodostus pääsykoodista on kalibroitu kestämään noin 80 millisekuntia. Tämä hidastaa brute-force hyökkäyksiä ja esim. 6-numeroisen pääsykoodin kaikkien vaihtoehtojen läpikäyntiin kuluu lähes vuorokausi. Saatu avain säilyy laitteen keskusmuistista, josta se poistetaan kun laite on laitettu lepotilaan. Tätä aikaa voidaan säätää laitteen asetuksista eli pääsykoodia ei välttämättä kysytä, jos laite avataan heti sulkemisen jälkeen uudestaan.

Laitteessa on asetus, jolla se voidaan asettaa tuhoamaan tyhjennettävässä muistissa olevat salausavaimet kymmenen virheellisen pääsykoodin jälkeen, joka käytännössä poistaa mahdollisuuden laitteen sisällön dekryptaamiseen. Pääsykoodin kysyminen myös hidastuu seuraavasti:

- 1-4 yrityksen jälkeen ei viivettä
- 5 yrityksen jälkeen 1 minuutti
- 6 yrityksen jälkeen 5 minuuttia
- 7-8 yrityksen jälkeen 15 minuuttia
- 9 yrityksen jälkeen 1 tuntiin

[ACLU:n blogissa](https://www.aclu.org/blog/free-future/one-fbis-major-claims-iphone-case-fraudulent) on argumentoitu, että tämän tuhoamismekanismin vaikutukset voitaisiin kiertää kopioimalla laitteen massamuistin sisältö (ja sitä myötä salausavain) ja palauttamalla kopio laitteeseen mahdollisten epäonnistuneiden yritysten jälkeen. Mielestäni kuitenkaan tälläkään tavalla ei pystytä poistamaan yritysten välistä viivettä, josta Secure Enclave huolehtii, ellei se sitten tallenna yritysten määrää samaiselle massamuistille.

TouchID on sormenjälkiskanneri, joka mahdollistaa sormenjäljen käyttämisen pääsykoodin lisäksi laitteen avaamiseen. Tämä mahdollistaa monimutkaisempien salasanojen käytön, kun käyttäjän ei tarvitse antaa pääsykoodia joka kerta halutessaan avata puhelimen lukituksesta. Salasanakirjautumiseen palaudutaan mm. jos

- Laite on juuri käynnistetty
- Laitetta ei ole avattu 48 tuntiin
- Laite on lukittu etälukitustoiminnolla
- Sormenjälkitunnistus on epäonnistunut 5 kertaa peräkkäin.

Applen mukaan todennäköisyys, että laitteelle opetettu sormi täsmää satunnaisesti valitun henkilön sormeen on 1/50000. Todennäköisyys, että laite saadaan auki satunnaisesti valittujen henkilöiden sormenjäljillä on siis 0,01%.

Kun sormenjälki skannataan TouchID-sensori ja Secure Enclave muodostavat ensin muilta osapuolilta salatun sessioavaimen, jolla salattu sormenjälkikuva siirretään Secure Enclaveen käsiteltäväksi. Tämä *data-at-move* -salaus varmistaa, että käyttöjärjestelmätason toimijat eivät voi sekaantua prosessiin. Sensorilta saatava rasteridata muutetaan Secure Enclavessa vektorimuotoon käyttäen ihonalaisten harjanteiden kulmiin perustuvaa analyysia (*subdermal ridge flow angle mapping*), joka on prosessina häviöllinen eli tallennettavasta lopputuloksesta ei voida palauttaa käyttäjän sormenjälkeä.

TouchID:n toiminta perustuu pääsykoodista muodostetun avaimen kryptaamiseen. Pääsykoodista johdettu avain toimii siis edelleen varsinaisena salausavaimena, mutta lukituksen yhteydessä se kryptataan heti TouchID:sta saadulla avaimella ja sormenjäljen tunnistuksen yhteydessä avataan uudelleen. Tämän johdetun avaimen poistaminen esim. 5 epäonnistuneen sormenjälkitunnistuksen jälkeen, johtaa pääsykoodikirjautumiseen palautumiseen.

## Tulossa ##

Seuraavassa osassa tarkastellaan tarkemmin levynsalauksen toimintaa ja erilaisia suojausluokkia. Lisäksi tutustutaan eri palveluiden kuten iMessagen ja Apple Payn tietoturvaan.
