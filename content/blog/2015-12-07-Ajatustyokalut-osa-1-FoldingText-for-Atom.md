+++
date = "2015-12-07T19:30:00+03:00"
draft = false
title = "Ajatustyökalut, osa 1: FoldingText for Atom"
slug = "Ajatustyokalut osa 1 FoldingText for Atom"
author = "anttim"
thumbnail = "/images/blog/categories/development.jpg"
+++

Tuumaustyökalut on jaettavissa karkeasti kahteen ryhmään: mindmap-sovellukset (tai ihan paperiversiokin), ja outlining-tyyppiset (paperilla niitä perinteisiä "ranskalaisia viivoja"). Tässä artikkelissa esittelen yhden viimeisimmistä löydöistäni, ehkä enemmän näppäimistöorientoituneille suunnatun Atom-editorin lisäosan, [FoldingText for Atomin](http://www.foldingtext.com/foldingtext-for-atom/).

FoldingText edustaa edellämainituista kategorioista outlining-työkaluja. Periaatteessahan ranskalaisia viivoja on helppo kirjoitella tekstieditoriin kuin tekstieditoriin, mutta FoldingText helpottaa esimerkiksi asioiden järjestämisessä ja sisäkkäisyyden manipuloinnissa.

## Asentaminen

FoldingText for Atom asentuu kuten mikä tahansa muukin Atom-lisäosa: Atomin Settings-näkymän Install-tabin kautta. Paketin nimi on `foldingtext-for-atom`.

## Käyttö

Asennuksen jälkeen Atomin File-valikkoon ilmestyy uusi valinta: `New Outline`. `New Outline`:n valitseminen luo uuden `.ftml`-tiedoston (FoldingTextin formaatti) ja asettaa Atomin FoldingText-tilaan. Tässä tilassa perustekstieditointi muuttuu outlinejen kirjoitusmoodiksi, ja outlinen kohtia voi lisätä kirjoittamalla asian ja painamalla enteriä. Sisennys onnistuu tabulaattorilla, sisennyksen pienennys backspacella.

FoldingTextissä voi nimensä mukaisesti taittaa sisennettyjä kohtia piiloon ja esiin jos halutaan esimerkiksi keskittyä johonkin tiettyyn ajatuspuun haaraan. Piilottaminen ja näyttäminen onnistuu joko klikkaamalla rivien alussa olevia pieniä kolmioita, tai siirtymällä navigointimoodiin painamalla `esc`-näppäintä, siirtämällä rivikohdistin nuolinäppäimillä taitettavalle riville ja painamalla `.`-näppäintä.

Jos navigointimoodissa painaa vielä kerran `esc`-näppäintä, fokus siirtyy hakutoimintoon ikkunan alareunaan. Haulla voi livefiltteröidä outlinedokumentin sisältöä.

![FoldingText for Atom](/images/blog/Ajatustyokalut-osa-1-FoldingText-for-Atom/foldingtext.gif)

FoldingText for Atomissa on monia muita näppäriä ominaisuuksia, kuten [tagien](https://jessegrosjean.gitbooks.io/foldingtext-for-atom-user-s-guide/content/tags.html), [statusten](https://jessegrosjean.gitbooks.io/foldingtext-for-atom-user-s-guide/content/statuses.html) ja [prioriteettien](https://jessegrosjean.gitbooks.io/foldingtext-for-atom-user-s-guide/content/priorities.html) liittäminen asioihin. [Koko dokumentaatio](https://jessegrosjean.gitbooks.io/foldingtext-for-atom-user-s-guide/content/) on selkeästi kirjoitettu ja hyvinkin läpikahlaamisen arvoinen.

## Tiedostomuoto

Mainitsemisen arvoinen asia FoldingTextissä on tiedostomuodon avoimuus. FoldingText tallentaa tiedostonsa .ftml-päätteellä, mutta jos tiedoston avaa vaikkapa Atomissa ei-FoldingText-muodossa (esim. vaihtamalla tiedostopääte .txt:ksi), huomataan että tiedoston sisältö on HTML:ää! Tämä on tärkeää, sillä monet ajatustenhallintatyökalut haluavat tallentaa sisällön proprietaryformaatissa, jonka lukeminen ei käykään niin helposti ja on omiaan aiheuttamaan ns. toimittajalukkoa.

## FoldingText

Tässä artikkelissa puhuttiin FoldingText for Atom -tuotteesta, joka on ominaisuuksiltaan vain leikkaus [FoldingTextin](http://www.foldingtext.com) ominaisuuksista. FoldingText on OS X:lle saatavilla oleva [markdown](https://daringfireball.net/projects/markdown/)-editori. Suosittelen tutustumaan, jos FoldingText for Atomin rajat tulevat vastaan!
