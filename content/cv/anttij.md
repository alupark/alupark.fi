+++
date = "2011-07-19T12:13:05+03:00"
draft = false
title = "CV - Antti Järvinen"
slug = "CV - Antti Jarvinen"
[blackfriday]
  fractions = false
+++

# Antti Järvinen

**Syntymäaika:** 9.12.1984

**Kotipaikka:** Tampere, Teisko

## Koulutus

- Luonnontieteiden kandidaatti, tietojenkäsittelytiede, Tampereen yliopisto, 2005-2008
- Ylioppilas, Sammon lukio, 2003

## Työkokemus

### Alupark Oy
- Software Consultant, 11/2015 –

Toimeksiantoja:

- 9/2016 –, terveys- ja hyvinvointialan B2B2C-järjestelmän kehittäminen. Toimin osana alustakehityksestä vastaavaa tiimiä tech lead/arkkitehti roolissa. Teknologiat: Java, Apache Storm, Apache Kafka, JavaScript, Node.js, MongoDB
- 3/2016 – 9/2016, terveys- ja hyvinvointialan B2C-järjestelmän kehittäminen. Toimin osana mobiili- ja verkkosovelluksille tarkoitettujen taustapalveluiden kehityksestä vastaavaa tiimiä. Teknologiat: Java, Spring Boot, Mongo ja pieneltä osin myös iOS ja Swift.
- 11/2015 – 3/2016, media-alan B2C-verkkosovelluksen kehittäminen. Vastasin teknisestä totetuksesta yhdessä toisen kehittäjän kanssa. Erityisvastuualueena integraatiot asiakkaan taustajärjestelmiin. Teknologiat: JavaScript, Node.js, React, Redux, AWS.

### Insta DefSec Oy
- Product Owner, 5/2014 – 11/2015
- Tech Lead, 5/2012 – 5/2014
- Software Engineer, 1/2011 – 5/2012

ERICA-järjestelmän (suomalainen hätäkeskustietojärjestelmä) kehitys-, määrittely- ja koulutustehtävät. Insta Response -tuotteen tuoteomistajana toimiminen.

### Symmetria Software Oy
- Software Developer, 9/2010 – 1/2011
- Project Manager, 8/2009 – 9/2010

Verkkokauppa-alustan kehitys ja verkkokauppaprojekteja.

### Ambientia Oy
- Software Developer, 2/2009 – 8/2009
- User Interface Designer, 11/2007 – 2/2009

Räätälöityjä järjestelmiä ja CMS-toteutuksia.

## Osaaminen
- **Ohjelmointikielet**: Java ja JavaScript. Lisäksi olen käyttänyt seuraavia ohjelmointikieliä: Scala, Groovy, Clojure, Go, Objective-C, Swift, PHP ja Prolog.
- **Dataprosessointi**: Apache Kafka, Apache Storm
- **Web-teknologiat**: HTML, CSS, SASS, nginx
- **Alustat ja kirjasto**: Node.js, React, Redux, jQuery, Spring Framework, Java FX, Grails.
- **Tietokannat**: MySQL/MariaDB, PostgreSQL ja MongoDB. Lisäksi olen käyttänyt seuraavia tietokantoja: Cassandra.
- **Integraatioteknologiat**: REST, SOAP, Corba.
- **GIS**: OpenLayers, paikkatietoratkaisut yleisesti
- **Menetelmät**: Scrum, Lean Startup
- **Muut**: Git, Mercurial (hg), Enterprise Architect


## Kurssit

- Managerial Accounting: Cost Behaviors, Systems, and Analysis, Coursera, 2015
- Breaker 101: An intensive online web security course (Cody Brocious), 2014
- Principles of Reactive Programming, Coursera, 2013
- Functional Programming Princples in Scala, Coursera, 2013
- Cryptography 1, Coursera, 2013
- Malicious Software and its Underground Economy: Two Sides to Every Story, Coursera, 2013

## Sertifikaatit

- Certified ScrumMaster, 2011
- Sun Certified Java Programmer, 2008

## Kielitaito

- Suomi, äidinkieli
- Englanti, kiitettävä
- Ruotsi, tyydyttävä

## Harrastukset

Vapaa-aikanani kuntoilen (suunnistus, juoksu, hiihto, triathlon). Erilaiset retket ja seikkailut ovat aina olleet minulle tärkeitä. Tavoitteenani on valloittaa vielä joskus kaikkien maiden korkeimmat vuorenhuiput. Lisäksi toimin partiolippukunta Teiska-Partiossa, jossa tehtäväni on järjestää erilaisia retkiä ja vaelluksia nuorille ja tarjota tätä kautta positiisia luonto- ja seikkailukokemuksia.
