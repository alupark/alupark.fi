HUGO_VERSION=0.15
HUGO_NAME=hugo_${HUGO_VERSION}_linux_amd64
HUGO_DOWNLOAD=${HUGO_NAME}.tar.gz

set -x
set -e

# Install Hugo if not already cached or upgrade an old version.
if [ ! -e $CIRCLE_BUILD_DIR/bin/hugo ] || ! [[ `hugo version` =~ v${HUGO_VERSION} ]]; then
  wget https://github.com/spf13/hugo/releases/download/v${HUGO_VERSION}/${HUGO_DOWNLOAD}
  tar xvzf ${HUGO_DOWNLOAD} -C $CIRCLE_BUILD_DIR/bin/
  mv bin/${HUGO_NAME}/${HUGO_NAME} bin/hugo
fi
