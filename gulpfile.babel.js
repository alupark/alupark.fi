import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';
import browserSync from 'browser-sync';
import del from 'del';
import run from 'gulp-run';
import Promise from 'bluebird';
import plumber from 'gulp-plumber';

const args = require('yargs').argv;

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

let debug = false;
let config = {};

gulp.task('styles', ['hugo'], () => {
    return gulp.src('dev/styles/**/*.scss')
        .pipe($.plumber())
        .pipe($.sourcemaps.init())
        .pipe($.sass.sync({
            outputStyle: 'expanded',
            precision: 10,
            includePaths: ['.']
        }).on('error', $.sass.logError))
        .pipe($.autoprefixer({browsers: ['last 1 version']}))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest('dist/styles'))
        .pipe(gulp.dest('dist/en/styles'))
        .pipe(reload({stream: true}));
});

gulp.task('html', ['styles'], () => {
    const assets = $.useref.assets({searchPath: ['.tmp', 'app', '.']});

    return gulp.src('dev/**/*.html')
        .pipe(assets)
        .pipe($.if('*.js', $.uglify()))
        .pipe($.if('*.css', $.minifyCss({compatibility: '*'})))
        .pipe(assets.restore())
        .pipe($.useref())
        .pipe($.if('*.html', $.minifyHtml({conditionals: true, loose: true})))
        .pipe(gulp.dest('dist'));
});

gulp.task('images', ['hugo'], () => {
    return gulp.src('dev/images/**/*')
        .pipe($.if($.if.isFile, $.cache($.imagemin({
            progressive: true,
            interlaced: true,
            // don't remove IDs from SVGs, they are often used
            // as hooks for embedding and styling
            svgoPlugins: [{cleanupIDs: false}]
        }))
        .on('error', function (err) {
            console.log(err);
            this.end();
        })))
        .pipe(gulp.dest('dist/images'))
        .pipe(gulp.dest('dist/en/images'));
});

gulp.task('extras', ['hugo'], () => {
    return gulp.src([
        `dev/*.*`
    ], {
        dot: true
    }).pipe(gulp.dest('dist'));
});

gulp.task('clean', () => {
  return del.sync(['.tmp', 'dist', 'dev']);
});

gulp.task('hugo', ['hugo-fi', 'hugo-en']);

gulp.task('hugo-fi', (cb) => {
    if(debug) {
        plumber().pipe(run(`hugo -v -D -d dev --config config_fi.toml`).exec('', () => cb()));
    } else {
        return run(`hugo -d dev --config config_fi.toml`).exec('');
    }
});

gulp.task('hugo-en', (cb) => {
    if(debug) {
        plumber().pipe(run(`hugo -v -D -d dev/en --config config_en.toml`).exec('', () => cb()));
    } else {
        return run(`hugo -d dev/en --config config_en.toml`).exec('');
    }
});

gulp.task('set-debug', (cb) => {
    debug = true;
    cb();
});

gulp.task('serve', ['set-debug', 'styles'], () => {
    browserSync({
        notify: false,
        port: 9000,
        server: {
            baseDir: ['.tmp', 'dev']
        }
    });

    gulp.watch([
        'content/**/*',
        'static/**/*',
        'layouts/**/*',
        'data/**/*'
], ['styles']);

    gulp.watch([
        'dev/**/*.html',
        'dev/scripts/**/*.js',
        'dev/images/**/*'
    ]).on('change', reload);
});

gulp.task('serve:dist', () => {
    browserSync({
        notify: false,
        port: 9000,
        server: {
            baseDir: ['dist']
        }
    });
});

gulp.task('scripts', ['hugo', 'html'], () => {
    return gulp.src([`dev/scripts/*.js`]).pipe(gulp.dest('dist/scripts')).pipe(gulp.dest('dist/en/scripts'));
});

gulp.task('build', ['html', 'images', 'scripts', 'extras'], (cb) => {
    return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('docker', ['clean', 'build'], () => {
    return run('docker build -t alupark/alupark.fi .').exec();
});

gulp.task('docker-run', ['docker'], () => {
    console.log("Running site. Go to http://localhost:8888")
    return run('docker run --rm --name alupark/alupark.fi -p 8888:80 alupark/alupark.fi').exec();
});

gulp.task('default', ['clean'], () => {
    gulp.start('build');
});
