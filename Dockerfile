from nginx

COPY dist /usr/share/nginx/html
COPY build/nginx.conf /etc/nginx/nginx.conf
COPY build/nginx_default.conf /etc/nginx/conf.d/default.conf
