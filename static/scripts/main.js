/* global document */

"use strict";

function ready(fn) {
    if (document.readyState !== "loading") {
        fn();
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

ready(function() {
    var menu = document.querySelector("#js-mobile-nav");
    var toggles = document.querySelectorAll(".js-toggle-menu");
    [].forEach.call(toggles, function(toggle) {
        toggle.addEventListener("click", function(e) {
            e.preventDefault();
            menu.classList.toggle("visible");
        });
    });
});
